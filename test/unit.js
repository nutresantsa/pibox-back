// Dependencias para pruebas
const chai = require('chai');
const chaiHttp = require('chai-http');
const expect = chai.expect;
chai.should();
chai.use(chaiHttp);

// API
const app = require('../app');

// Dependencia para leer imagen
const fs = require('fs');
const imagePath = './test/mocks/1234567890123_A1C1.png';

// Mocks
const userListMock = require('./mocks/user_list.json');
const userMock = require('./mocks/user.json');
const categoryMock = require('./mocks/category.json');
const lineMock = require('./mocks/line.json');
const brandMock = require('./mocks/brand.json');
const companyMock = require('./mocks/company.json');
const imageMock = require('./mocks/imageData.json');

// Scripts
const auth = require('../api/auth/securityJWT');
const validateData = require('../api/tools/validations/validateData');
const env = require('../api/lib/setupEnv');
const job = require('../api/tools/jobs/job');

// Token de autenticación JWT
var token;
// Id de cada entidad
var userID, userID2, roleID, roleAdminID, roleMarketingID, companyID, categoryID, lineID, brandID, imagesID = [];
// Datos de apoyo en pruebas
var fileName;

// FUNCIONES DE securityJWT
describe('SEGURIDAD JWT', () => {

  describe("Función validateUser", () => {
    it("Debe devolver un false", () => {
      // Se obtiene el password como hash
      const user = { password: userListMock[1].password };
      const password = '4321';
      const sw = auth.validateUser(user, password);
      expect(sw.resp).to.equal(false);
    })
  });

  describe("Función validateUser", () => {
    it("Debe devolver un true", () => {
      // Se obtiene el password como hash
      const user = { password: userListMock[1].password };
      const password = '1234';
      const sw = auth.validateUser(user, password);
      expect(sw.resp).to.equal(true);
      token = sw.msg.token;
    })
  });

  describe("Función verifyToken", () => {
    it("Debe devolver un true", () => {
      const req = { headers: { 'x-access-token': token } }
      const sw = auth.verifyToken(req);
      expect(sw.resp).to.equal(true);
    })
  });

  describe("Función verifyToken", () => {
    it("Debe devolver un false", () => {
      const req = { headers: { 'x-access-token': '1234' } }
      const sw = auth.verifyToken(req);
      expect(sw.resp).to.equal(false);
    })
  });

  describe("Función verifyToken", () => {
    it("Debe devolver un false", () => {
      const req = { headers: { 'x-access-token': '' } }
      const sw = auth.verifyToken(req);
      expect(sw.resp).to.equal(false);
    })
  });

});

// FUNCIONES DE validateData
describe('VALIDACIÓN DE DATO', () => {

  describe("Funcion validateImageName", () => {
    it("Debe devolver un false", () => {
      // Falla por no tener el tamaño correcto
      let nameFile = imageMock[0].nameFile + "1";
      const sw = validateData.validateImageName(nameFile);
      expect(sw).to.equal(false);
    })
  });

  describe("Funcion validateImageName", () => {
    it("Debe devolver un false", () => {
      // Falla por no estar separado por guion bajo
      let nameFile = imageMock[0].nameFile;
      nameFile = nameFile.replace('_', '-');
      const sw = validateData.validateImageName(nameFile);
      expect(sw).to.equal(false);
    })
  });

  describe("Funcion validateImageName", () => {
    it("Debe devolver un false", () => {
      // Falla por tener letras en gtin
      let nameFile = imageMock[0].nameFile;
      let num = nameFile.charAt(0);
      nameFile = nameFile.replace(num, 'a');
      const sw = validateData.validateImageName(nameFile);
      expect(sw).to.equal(false);
    })
  });

  describe("Funcion validateImageName", () => {
    it("Debe devolver un false", () => {
      // Falla por tipo de imagen
      let nameFile = imageMock[0].nameFile;
      let type = nameFile.charAt(14);
      nameFile = nameFile.replace(type, 'C');
      const sw = validateData.validateImageName(nameFile);
      expect(sw).to.equal(false);
    })
  });

  describe("Funcion validateImageName", () => {
    it("Debe devolver un false", () => {
      // Falla por tipo de cara
      let nameFile = imageMock[0].nameFile;
      let face = nameFile.charAt(14);
      nameFile = nameFile.replace(face, '3');
      const sw = validateData.validateImageName(nameFile);
      expect(sw).to.equal(false);
    })
  });

  describe("Funcion validateImageName", () => {
    it("Debe devolver un false", () => {
      // Falla por tipo angulo
      let nameFile = imageMock[0].nameFile;
      let angle = nameFile.charAt(14);
      nameFile = nameFile.replace(angle, 'C');
      const sw = validateData.validateImageName(nameFile);
      expect(sw).to.equal(false);
    })
  });

  describe("Funcion validateImageName", () => {
    it("Debe devolver un false", () => {
      // Falla por tipo de estado
      let nameFile = imageMock[0].nameFile;
      let state = nameFile.charAt(14);
      nameFile = nameFile.replace(state, '2');
      const sw = validateData.validateImageName(nameFile);
      expect(sw).to.equal(false);
    })
  });

  describe("Funcion validateImageName", () => {
    it("Debe devolver un true", () => {
      let nameFile = imageMock[0].nameFile;
      const sw = validateData.validateImageName(nameFile);
      expect(sw).to.equal(true);
    })
  });

  describe("Funcion validateImageType", () => {
    it("Debe devolver un true", () => {
      const type = 'png';
      const sw = validateData.validateImageType(type);
      expect(sw).to.equal(true);
    })
  });

  describe("Funcion isEmpty", () => {
    it("Debe devolver un true", () => {
      let data = '';
      const sw = validateData.isEmpty(data);
      expect(sw).to.equal(true);
    })
  });

});

// FUNCIONES DE setupEnv
describe('CONFIGURACIÓN DE AMBIENTE', () => {

  describe("Función getKeyPath", () => {
    it("Debe devolver la ruta de la llave", () => {
      const keyPath = env.getKeyPath();
      expect(keyPath).to.be.a('string');
    })
  });

  describe("Función getBucketName", () => {
    it("Debe devolver el nombre del bucket", () => {
      const bucket = env.getBucketName();
      expect(bucket).to.be.a('string');
    })
  });

  describe("Función getFront", () => {
    it("Debe devolver la URL del front", () => {
      const front = env.getFront();
      expect(front).to.be.a('string');
    })
  });

  describe("Función cleanTemp", () => {
    it("Debe obtener una función", () => {
      let type = typeof job.cleanTemp;
      expect(type).to.equal('function');
    })
  });

});

// LOGIN
describe('OBTENER TOKEN', () => {
  describe('POST /user/login ', function () {
    it('Debe devolver un token', function (done) {
      // Obtener usuario para login, debe existir en la base de datos
      const user = userListMock[0];
      const login = {
        user: user.email,
        password: user.password
      }
      chai.request(app)
        .post('/user/login')
        .send(login)
        .end((err, res) => {
          if (err) done(err);
          expect(res.body.resp).to.equal(true);
          token = res.body.msg.token;
          userID = res.body.msg.user.id;
          done();
        });
    });
  });
});

// LISTAR ENTIDADES
describe('OBTENER TODOS LOS REGISTROS', () => {

  describe('GET /user/getUsers', () => {
    it('Debe devolver una lista de usuarios', done => {
      chai.request(app)
        .get('/user/getUsers')
        .set('x-access-token', token)
        .end((err, res) => {
          if (err) done(err);
          expect(res.body.resp).to.equal(true);
          userID = res.body.msg[0].id;
          done();
        });
    });
  });

  describe('GET /user/getRoles', () => {
    it('Debe devolver una lista de roles', done => {
      chai.request(app)
        .get('/user/getRoles')
        .set('x-access-token', token)
        .end((err, res) => {
          if (err) done(err);
          expect(res.body.resp).to.equal(true);
          let roleList = res.body.msg;
          // Guardar el ID del admin para crear y del mercadeo para actualizar usuario
          roleList.forEach(element => {
            if (element.role === 'Administrador') {
              roleAdminID = element.id;
            }
            if (element.role === 'Mercadeo') {
              roleMarketingID = element.id;
            }
          });
          done();
        });
    });
  });

  describe('GET /management/getCompanies', () => {
    it('Debe devolver una lista de compañías', done => {
      chai.request(app)
        .get('/management/getCompanies')
        .set('x-access-token', token)
        .end((err, res) => {
          if (err) done(err);
          expect(res.body.resp).to.equal(true);
          companyID = res.body.msg[0].id;
          done();
        });
    });
  });

  describe('GET /management/getCategories', () => {
    it('Debe devolver una lista de categorías', done => {
      chai.request(app)
        .get('/management/getCategories')
        .set('x-access-token', token)
        .end((err, res) => {
          if (err) done(err);
          expect(res.body.resp).to.equal(true);
          categoryID = res.body.msg[0].id;
          done();
        });
    });
  });

  describe('GET /management/getLines', () => {
    it('Debe devolver una lista de líneas', done => {
      chai.request(app)
        .get('/management/getLines')
        .set('x-access-token', token)
        .end((err, res) => {
          if (err) done(err);
          expect(res.body.resp).to.equal(true);
          lineID = res.body.msg[0].id;
          done();
        });
    });
  });

  describe('GET /management/getBrands', () => {
    it('Debe devolver una lista de marcas', done => {
      chai.request(app)
        .get('/management/getBrands')
        .set('x-access-token', token)
        .end((err, res) => {
          if (err) done(err);
          expect(res.body.resp).to.equal(true);
          brandID = res.body.msg[0].id;
          done();
        });
    });
  });

});

// LISTAR ENTIDADES ACTIVAS
describe('OBTENER TODOS LOS REGISTROS ACTIVOS', () => {

  describe('GET /management/getActiveCategories', () => {
    it('Debe devolver una lista de categorías activas', done => {
      chai.request(app)
        .get('/management/getActiveCategories')
        .set('x-access-token', token)
        .end((err, res) => {
          if (err) done(err);
          expect(res.body.resp).to.equal(true);
          categoryID = res.body.msg[0].id;
          done();
        });
    });
  });

  describe('GET /management/getActiveLines', () => {
    it('Debe devolver una lista de líneas activas', done => {
      chai.request(app)
        .get('/management/getActiveLines')
        .set('x-access-token', token)
        .end((err, res) => {
          if (err) done(err);
          expect(res.body.resp).to.equal(true);
          lineID = res.body.msg[0].id;
          done();
        });
    });
  });

  describe('GET /management/getActiveBrands', () => {
    it('Debe devolver una lista de marcas activas', done => {
      chai.request(app)
        .get('/management/getActiveBrands')
        .set('x-access-token', token)
        .end((err, res) => {
          if (err) done(err);
          expect(res.body.resp).to.equal(true);
          brandID = res.body.msg[0].id;
          done();
        });
    });
  });

});

// OBTENER UN REGISTRO
describe('OBTENER UN REGISTRO', () => {

  describe('GET /user/getUser', () => {
    it('Debe devolver un usuario', function (done) {
      let id = userID;
      chai.request(app)
        .get('/user/getUser')
        .set('x-access-token', token)
        .set('id', id)
        .end((err, res) => {
          if (err) done(err);
          res.should.have.status(200);
          done();
        });
    })
  });

  describe('GET /user/getRole', () => {
    it('Debe devolver un rol', function (done) {
      let id = roleAdminID;
      chai.request(app)
        .get('/user/getRole')
        .set('x-access-token', token)
        .set('id', id)
        .end((err, res) => {
          if (err) done(err);
          res.should.have.status(200);
          done();
        });
    })
  });

  describe('GET /management/getCategory', () => {
    it('Debe devolver una categoría', function (done) {
      let id = categoryID;
      chai.request(app)
        .get('/management/getCategory')
        .set('x-access-token', token)
        .set('id', id)
        .end((err, res) => {
          if (err) done(err);
          res.should.have.status(200);
          done();
        });
    })
  });

  describe('GET /management/getLine', () => {
    it('Debe devolver una línea', function (done) {
      let id = lineID;
      chai.request(app)
        .get('/management/getLine')
        .set('x-access-token', token)
        .set('id', id)
        .end((err, res) => {
          if (err) done(err);
          res.should.have.status(200);
          done();
        });
    })
  });

  describe('GET /management/getBrand', () => {
    it('Debe devolver una marca', function (done) {
      let id = brandID;
      chai.request(app)
        .get('/management/getBrand')
        .set('x-access-token', token)
        .set('id', id)
        .end((err, res) => {
          if (err) done(err);
          res.should.have.status(200);
          done();
        });
    })
  });

  describe('GET /management/getCompany', () => {
    it('Debe devolver una compañia', function (done) {
      let id = companyID;
      chai.request(app)
        .get('/management/getCompany')
        .set('x-access-token', token)
        .set('id', id)
        .end((err, res) => {
          if (err) done(err);
          res.should.have.status(200);
          done();
        });
    })
  });

});

// ACCIONES DE USUARIO
describe('ACCIONES DE USUARIO', () => {

  describe('POST /user/create ', function () {
    it('Debe crear un usuario administrador', function (done) {
      let data = userMock;
      data.role = roleAdminID;
      data.company = companyID;
      chai.request(app)
        .post('/user/create')
        .set('x-access-token', token)
        .send(data)
        .end((err, res) => {
          if (err) done(err);
          expect(res.body.resp).to.equal(true);
          userID2 = res.body.msg.id;
          done();
        });
    });
  });

  describe('POST /user/updateUser ', function () {
    it('Debe cambiar el usuario admin por mercadeo', function (done) {
      let id = userID2;
      let data = userMock;
      data.firstname = "name delete";
      data.role = roleMarketingID;
      chai.request(app)
        .put('/user/updateUser')
        .set('x-access-token', token)
        .set('id', id)
        .send(data)
        .end((err, res) => {
          if (err) done(err);
          expect(res.body.resp).to.equal(true);
          done();
        });
    });
  });

  describe('DELETE /user/deleteUser', () => {
    it('Debe eliminar un usuario', function (done) {
      let id = userID2;
      chai.request(app)
        .delete('/user/deleteUser')
        .set('x-access-token', token)
        .set('id', id)
        .end((err, res) => {
          if (err) done(err);
          expect(res.body.resp).to.equal(true);
          done();
        })
    })
  });

  describe('POST /user/createRole ', function () {
    it('Debe crear un rol', function (done) {
      let data = userListMock[0].role;
      chai.request(app)
        .post('/user/createRole')
        .set('x-access-token', token)
        .send(data)
        .end((err, res) => {
          if (err) done(err);
          expect(res.body.resp).to.equal(true);
          roleID = res.body.msg.id;
          done();
        });
    });
  });

  describe('DELETE /user/deleteRole', () => {
    it('Debe eliminar un rol', function (done) {
      let id = roleID;
      chai.request(app)
        .delete('/user/deleteRole')
        .set('x-access-token', token)
        .set('id', id)
        .end((err, res) => {
          if (err) done(err);
          expect(res.body.resp).to.equal(true);
          done();
        })
    })
  });

});

// CREAR, ACTUALIZAR Y ELIMINAR ENTIDADES
describe('CREAR, ACTUALIZAR Y ELIMINAR ENTIDADES', () => {

  describe('POST /management/createCategory ', function () {
    it('Debe crear una entidad de categoría', function (done) {
      let data = categoryMock;
      chai.request(app)
        .post('/management/createCategory')
        .set('x-access-token', token)
        .send(data)
        .end((err, res) => {
          if (err) done(err);
          expect(res.body.resp).to.equal(true);
          categoryID = res.body.msg.id;
          done();
        });
    });
  });

  describe('PUT /management/updateCategory', () => {
    it('Debe desactivar una entidad de categoría', function (done) {
      let data = categoryMock;
      data.active = false;
      let id = categoryID;
      chai.request(app)
        .put('/management/updateCategory')
        .set('x-access-token', token)
        .set('id', id)
        .send(data)
        .end((err, res) => {
          if (err) done(err);
          expect(res.body.resp).to.equal(true);
          done();
        })
    })
  });

  describe('DELETE /management/deleteCategory', () => {
    it('Debe eliminar una entidad de categoría', function (done) {
      let id = categoryID;
      chai.request(app)
        .delete('/management/deleteCategory')
        .set('x-access-token', token)
        .set('id', id)
        .end((err, res) => {
          if (err) done(err);
          expect(res.body.resp).to.equal(true);
          done();
        })
    })
  });

  describe('POST /management/createLine ', function () {
    it('Debe crear una entidad de línea', function (done) {
      let data = lineMock;
      chai.request(app)
        .post('/management/createLine')
        .set('x-access-token', token)
        .send(data)
        .end((err, res) => {
          if (err) done(err);
          expect(res.statusCode).to.equal(200);
          lineID = res.body.msg.id;
          done();
        });
    });
  });

  describe('PUT /management/updateLine', () => {
    it('Debe desactivar una entidad de línea', function (done) {
      let data = lineMock;
      data.active = false;
      let id = lineID;
      chai.request(app)
        .put('/management/updateLine')
        .set('x-access-token', token)
        .set('id', id)
        .send(data)
        .end((err, res) => {
          if (err) done(err);
          expect(res.body.resp).to.equal(true);
          done();
        })
    })
  });

  describe('DELETE /management/deleteLine', () => {
    it('Debe eliminar una entidad de línea', function (done) {
      let id = lineID;
      chai.request(app)
        .delete('/management/deleteLine')
        .set('x-access-token', token)
        .set('id', id)
        .end((err, res) => {
          if (err) done(err);
          expect(res.body.resp).to.equal(true);
          done();
        })
    })
  });

  describe('POST /management/createBrand ', function () {
    it('Debe crear una entidad de marca', function (done) {
      let data = brandMock;
      chai.request(app)
        .post('/management/createBrand')
        .set('x-access-token', token)
        .send(data)
        .end((err, res) => {
          if (err) done(err);
          expect(res.body.resp).to.equal(true);
          brandID = res.body.msg.id;
          done();
        });
    });
  });

  describe('PUT /management/updateBrand', () => {
    it('Debe desactivar una entidad de marca', function (done) {
      let data = brandMock;
      data.active = false;
      let id = brandID;
      chai.request(app)
        .put('/management/updateBrand')
        .set('x-access-token', token)
        .set('id', id)
        .send(data)
        .end((err, res) => {
          if (err) done(err);
          expect(res.body.resp).to.equal(true);
          done();
        })
    })
  });

  describe('DELETE /management/deleteBrand', () => {
    it('Debe eliminar una entidad de marca', function (done) {
      let id = brandID;
      chai.request(app)
        .delete('/management/deleteBrand')
        .set('x-access-token', token)
        .set('id', id)
        .end((err, res) => {
          if (err) done(err);
          expect(res.body.resp).to.equal(true);
          done();
        })
    })
  });

  describe('POST /management/createCompany ', function () {
    it('Debe crear una entidad de compañia', function (done) {
      let data = companyMock;
      chai.request(app)
        .post('/management/createCompany')
        .set('x-access-token', token)
        .send(data)
        .end((err, res) => {
          if (err) done(err);
          expect(res.body.resp).to.equal(true);
          companyID = res.body.msg.id;
          done();
        });
    });
  });

  describe('DELETE /management/deleteCompany', () => {
    it('Debe eliminar una entidad de compañia', function (done) {
      let id = companyID;
      chai.request(app)
        .delete('/management/deleteCompany')
        .set('x-access-token', token)
        .set('id', id)
        .end((err, res) => {
          if (err) done(err);
          expect(res.body.resp).to.equal(true);
          done();
        })
    })
  });

});

// ACCIONES PARA IMÁGENES
describe('ACCIONES PARA IMÁGENES', () => {

  // Subir imagen principal
  describe('POST /storage/upload ', function () {
    it('Debe devolver un true', function (done) {
      let ruta = imagePath;
      let imageData = imageMock[0];

      // Leer imagen en base64
      let imageBase64 = fs.readFileSync(ruta, { encoding: 'base64' });
      imageData.imageBase64 = imageBase64;

      chai.request(app)
        .post('/storage/upload')
        .set('x-access-token', token)
        .send(imageData)
        .end((err, res) => {
          if (err) done(err);
          expect(res.body.resp).to.equal(true);
          // Guardar ID para usar servicio de borrar
          imagesID.push(res.body.msg.id);
          done();
        });
    });
  });

  // Subir nueva versión de imagen
  describe('POST /storage/upload ', function () {
    it('Debe devolver un true', function (done) {
      let ruta = imagePath;
      let imageData = imageMock[0];

      // Leer imagen en base64
      let imageBase64 = fs.readFileSync(ruta, { encoding: 'base64' });
      imageData.imageBase64 = imageBase64;

      chai.request(app)
        .post('/storage/upload')
        .set('x-access-token', token)
        .send(imageData)
        .end((err, res) => {
          if (err) done(err);
          expect(res.body.resp).to.equal(true);
          // Guardar ID para usar servicio de borrar
          imagesID.push(res.body.msg.id);
          done();
        });
    });
  });

  describe('POST /storage/upload ', function () {
    it('Debe devolver un false', function (done) {
      let imageData = imageMock[0];
      // Falla por no cumplir el estandar de nombramiento
      imageData.nameFile = '1234567890123_A1C5';
      chai.request(app)
        .post('/storage/upload')
        .set('x-access-token', token)
        .send(imageData)
        .end((err, res) => {
          if (err) done(err);
          expect(res.body.resp).to.equal(false);
          done();
        });
    });
  });

  describe('POST /storage/findImagesWithFilter ', function () {
    it('Debe devolver un true', function (done) {
      let filter = { gtin: "1234567890123" }
      chai.request(app)
        .post('/storage/findImagesWithFilter')
        .set('x-access-token', token)
        .send(filter)
        .end((err, res) => {
          if (err) done(err);
          expect(res.body.resp).to.equal(true);
          done();
        });
    });
  });

  describe('GET /storage/downloadAllFolder ', function () {
    it('Debe devolver el nombre de un archivo zip', function (done) {
      let ruta = imageMock[0].routeFile;
      chai.request(app)
        .get('/storage/downloadAllFolder')
        .set('x-access-token', token)
        .set('ruta', ruta)
        .end((err, res) => {
          if (err) done(err);
          fileName = res.body.msg;
          let sw = fileName.includes('.zip');
          expect(sw).to.equal(true);
          done();
        });
    });
  });

  describe('GET /storage/generateReport ', function () {
    it('Debe devolver el nombre de un archivo excel', function (done) {
      chai.request(app)
        .get('/storage/generateReport')
        .set('x-access-token', token)
        .end((err, res) => {
          if (err) done(err);
          let sw = res.body.msg.includes('.xlsx');
          expect(sw).to.equal(true);
          done();
        });
    });
  });

  describe('GET /storage/downloadFile ', function () {
    it('Debe devolver un false', function (done) {
      let badFileName = 'archivo.zip';
      chai.request(app)
        .get('/storage/downloadFile')
        .set('x-access-token', token)
        .set('filename', badFileName)
        .end((err, res) => {
          if (err) done(err);
          expect(res.body.resp).to.equal(false);
          done();
        });
    });
  });

  describe('GET /storage/downloadFile ', function () {
    it('Debe devolver un true', function (done) {
      chai.request(app)
        .get('/storage/downloadFile')
        .set('x-access-token', token)
        .set('filename', fileName)
        .end((err, res) => {
          if (err) done(err);
          expect(res.body.resp).to.equal(true);
          done();
        });
    });
  });

  describe('POST /storage/imgConvert ', function () {
    it('Debe devolver un true', function (done) {
      let imageData = imageMock[1];
      chai.request(app)
        .post('/storage/imgConvert')
        .set('x-access-token', token)
        .send(imageData)
        .end((err, res) => {
          if (err) done(err);
          expect(res.body.resp).to.equal(true);
          done();
        });
    });
  });

  // Eliminar las dos imagénes cargadas en la prueba
  describe('DELETE /storage/deleteImages ', function () {
    it('Debe devolver un true', function (done) {
      let imgToDelete = { listImagesID: imagesID };
      chai.request(app)
        .post('/storage/deleteImages')
        .set('x-access-token', token)
        .send(imgToDelete)
        .end((err, res) => {
          if (err) done(err);
          expect(res.body.resp).to.equal(true);
          done();
        });
    });
  });

  describe('DELETE /storage/deleteImages ', function () {
    it('Debe devolver un false', function (done) {
      let imgToDelete = { listImagesID: imagesID };
      chai.request(app)
        .post('/storage/deleteImages')
        .set('x-access-token', token)
        .send(imgToDelete)
        .end((err, res) => {
          if (err) done(err);
          expect(res.body.resp).to.equal(false);
          done();
        });
    });
  });

});

describe('JOB', () => {

  describe("Función cleanTemp", () => {
    it("Debe obtener una función", () => {
      let type = typeof job.cleanTemp;
      expect(type).to.equal('function');
    })
  });

  describe("Función deleteFiles", async () => {
    it("Debe devolver un true", async () => {
      let sw = await job.deleteFiles();
      expect(sw).to.equal(true);
    })
  });

});
