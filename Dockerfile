FROM node:10

# Create app directory
WORKDIR /usr/src/pibox

COPY package*.json ./

RUN npm install

# Bundle app source
COPY . .

EXPOSE 1234
CMD [ "npm", "start" ]