/**
 * @fileoverview Codigo principal de la API para la creacion del servidor con las rutas
 * @version    1.0.0
 * @author     Santiago Gonzalez Acevedo <santiago.gonzalez@netwconsulting.com>
 * @copyright  New Inntech S.A.S
 * History
 * v1.1 – API principal y rutas
 */
'use strict';

//INICIO: Dependencias Swagger
const SwaggerExpress = require('swagger-express-mw');
const SwaggerUi = require('swagger-tools/middleware/swagger-ui');

/**************************
 * INCIO DEPENDENCIAS API  *
 **************************/
const express = require('express');
const basicAuth = require('express-basic-auth');
const bodyParser = require('body-parser');
const cors = require('cors');
const fs = require('fs');
const job = require('./api/tools/jobs/job');
/**************************
 * FIN DEPENDENCIAS API   *
 **************************/

// Conexión con Google
const gcp = require('./api/lib/connectGCP');
// Configuración de api
const config = require('./api/config/config');
// Configuración de ambiente
const env = require('./api/lib/setupEnv');

// IMPORTANTE!!! Crear carpeta para archivos temporales y carpeta para llave de acceso
let dir = './api/temp';
if (!fs.existsSync(dir)) fs.mkdirSync(dir);
dir = './api/config/key';
if (!fs.existsSync(dir)) fs.mkdirSync(dir); // Guardar llave en esta carpeta

// Iniciar servidor
const app = express();
const port = process.env.PORT || 1234;

// Conección a GCP
gcp.conectionDatastore();

// Rutas API
const user = require('./api/routes/user.route');
const storage = require('./api/routes/storage.route');
const management = require('./api/routes/management.route');

app.use(bodyParser.json({ limit: '50mb' }));
app.use(bodyParser.urlencoded({ limit: '50mb', extended: false }));

// Establecer origin cors
let front = env.getFront();
let corsOptions = { origin: front };
app.use(cors(corsOptions));

app.get('/', (req, res) => { res.send(`PiBox API version ${config.version}`); });
app.use('/user', user);
app.use('/storage', storage);
app.use('/management', management);

// Job para eliminar archivos colados cada domingo a las 23:59:00
job.cleanTemp();

// Asignar email y clave para la autenticación de la documentación
function securityDoc() {
  let userDoc = {};
  userDoc[config.basicAuth.userDoc] = config.basicAuth.passDoc;
  // Seguridad para documentación con Basic Auth
  app.use(basicAuth({
    users: userDoc,
    challenge: true,
    realm: 'Imb4T3st4pp'
  }));
}

const swaggerConfig = { appRoot: __dirname };

// Configuración de documentación
SwaggerExpress.create(swaggerConfig, function (err, swaggerExpress) {
  if (err) { throw err; }
  securityDoc();
  //Sirve para correr el portal de Swagger y mostrar la documentacion de los servicios.
  app.use(SwaggerUi(swaggerExpress.runner.swagger));
  // install middleware
  swaggerExpress.register(app);
});

// Se exporta para poder correr las pruebas de integración
module.exports = app.listen(port, () => { console.log(`Server is up and running on port number ${port}`); });
