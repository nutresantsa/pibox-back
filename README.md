﻿# Pibox-Back

___

# PIBOX

La solución **PiBox** tiene como objetivo proveer un lugar centralizado para recopilar, gestionar y consultar las imágenes de los productos de venta en línea. Esta contendrá la última versión de cada imagen de cada uno de los productos en todas sus presentaciones.
___

# Proyecto Backend [![](api/tools/images/nodejs_64x64.png)](https://nodejs.org/en/)

Microservicio creado con NodeJS que se encargará de procesar y proveer los datos de la aplicación. Se usan los servicios que ofrece GCP tales como:

- Google Cloud Datastore para la persistencia de datos.
- Google Cloud Storage para almacenar los archivos.
- Google Cloud IAM para la autenticación.

## Tecnologías

Para construir la API se uso:

- NodeJS
- Cloud SDK

## Librerías

**Dependencias:**

- @google-cloud/datastore
- @google-cloud/storage
- archiver
- bcrypt-nodejs
- cors
- crypto
- dotenv
- excel4node
- express
- express-basic-auth
- fs
- gstore-node
- jimp
- jsonwebtoken
- node-cron
- stream
- swagger-express-mw
- swagger-tools
- uniqid

**Dependencias de desarrollo:**

- chai
- chai-http
- mocha
- nodemon
- nyc

## Features 📋

- Consultar, subir, eliminar, actualizar y descargar imágenes.
- Administración de usuarios.
- Gestión de entidades.

## Arquitectura 🧱

![1]:

[1]: api/tools/images/Arquitectura_PiBoxGCP.png

## Configuración 🔧

Se requiere [NodeJS](https://nodejs.org/) v10+ para correr.
Instale las dependencias y devDependencies:

```sh
cd pibox-back
npm install
```

## Ejecución ⚙️

⚠ La llave de autenticación debe estar en la ruta **./api/config/key/**

**Para probar el proyecto en local:**

Asegurarse que el archivo **config.js** en los campos *key_dev* y *bucketName_dev* correspondan al nombre de la llave y del bucket respectivamente.

```sh
module.exports = {
  'key_dev': 'nombreDeLlave.json',
  'bucketName_dev': 'miBucket',
   ...
```

Iniciar el servidor.

```sh
npm start
```

Vera un mensaje confirmando el inició:

```sh
Server is up and running on port number 1234
```

**Para probar en producción:**

Asegurarse que el archivo **app.yaml** en los campos *name_sa*  y *bucketName* correspondan al nombre de la llave y del bucket respectivamente.

```sh
env_variables:
  name_sa: nombreDeLlave.json
  bucketName: miBucket
```

Desplegar en GCP.

___

## Pruebas 🧪

### Pruebas unitarias

Para correr las pruebas utilice el comando:

```sh
npm run unit-tests
```

Para generar la cobertura utilice el comando:

```sh
npm run coverage-u
```

### Pruebas de integración

Para correr las pruebas utilice el comando:

```sh
npm run integration-tests
```

El reporte de cobertura se generaran en la ruta *./test/reports/unit/*

## Documentación 📖

Nuestra API cuenta con una [documentación](http://localhost:1234/docs/) en donde puedes encontrar mucho más de cómo utilizar este proyecto.

Para acceder utiliza las credenciales:
**Nombre de usuario:** *piboxdoc*
**Contraseña:** *1234*
___

## Errores conocidos ❌

Lista de errores y su metodo de correción:

- Al desplegar en GCP hay carpetas que se ignoran si estan vacias. Para prevenir esto se asegura a traves de código que estas carpetas existan una vez ejecute el servidor, si no existen, se crean automaticamente.

- Establecer correctamente los permisos en GCP para la cuenta de usuario o cuenta de GCP que va a crear la llave de acceso a los servicios de App Engine.

___

## Autores 👨‍💻 👩‍💻

Equipo de consultoria **NewInntech**

- **Santiago Gonzalez** - *Arquitecto y desarrollador* -
    Email empresarial: santiago.gonzalez@netwconsulting.com
    [Linkedin](https://www.linkedin.com/in/santiagosk80/)
- **Sebastian Cardona** - *Desarrollador* -
    Email empresarial: sebastian.cardona@gruponetw.com
    [Linkedin](https://www.linkedin.com/in/sebastiancardonaloaiza/)
