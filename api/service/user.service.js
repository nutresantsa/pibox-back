// Autenticación JWT
const auth = require('../auth/securityJWT');

// Modelos
const User = require('../models/user.model');
const Role = require('../models/role.model');
const Company = require('../models/company.model');

// Servicio general
const Common = require('../service/common.service');

const bcrypt = require('bcrypt-nodejs');

// Validar compañia.
const processCompany = async (companyId) => {
    let res = { code: 400, msg: {} };

    let userData = {};

    // Si se envia el ID de compañia se valida y asigna
    let entityCompany = await Common.getModel(Company, companyId);
    if (entityCompany.resp === false) {
        res.msg = { resp: false, msg: `La compañia con id ${companyId} no existe.` };
    } else {
        // Asignar la entidad compañia como una propiedad del usuario
        userData.company = entityCompany.msg.entityKey;
        res.msg = userData;
        res.code = 200;
    }
    return res;
}

// Validar si el id enviado pertenece a un rol existente
const processRol = async (dataRole) => {
    let res = { code: 400, msg: {} };

    let userData = {};
    let roleId = dataRole; // Obtener el id del rol

    let role = await Common.getModel(Role, roleId);
    if (role.resp === false) {
        res.msg = { resp: false, msg: `El rol con id ${roleId} no existe.` };
        return res;
    }

    // Asignar la entidad rol como una propiedad del usuario
    userData.role = role.msg.entityKey;
    res.code = 200;
    res.msg = userData;
    return res;
}

/**
 * @function hashPassword
 * @description Permite guardar la clave de forma segura en la base de datos
 */
async function hashPassword(password) {
    let encryptPass = '';
    let createHash = new Promise((resolve, reject) => {
        bcrypt.genSalt(5, function onSalt(err, salt) {
            if (err) reject(err);
            bcrypt.hash(password, salt, null,
                function onHash(err, hash) {
                    if (err) reject(err);
                    encryptPass = hash;
                    resolve();
                });
        });
    });
    await createHash;
    return encryptPass;
}


const getLogin = async (email, password) => {
    let res = { code: 0, msg: {} };
    const filter = { filters: [['email', email]] };
    // Realizar filtro para buscar el usuario por email
    let listUser = await Common.listModelsWithFilter(User, filter);

    // Valida si se devolvio un usuario
    if (listUser.resp === false) {
        res.code = 400;
        res.msg = { resp: false, msg: "Usuario no registrado." }
        return res;
    }

    if (listUser.msg[0].active === false) {
        res.code = 200;
        res.msg = { resp: false, msg: "Usuario inactivo." }
        return res;
    }

    // Valida las credenciales del usuario, se envia la entidad de Datastore
    let validationUser = auth.validateUser(listUser.msg[0], password);
    if (validationUser.resp === true) {
        res.code = 200;
    } else {
        res.code = 400;
    }
    res.msg = validationUser;
    return res;
}

const createUser = async (data) => {
    let res = { resp: false, code: 0, msg: {} };

    // Validar si se envio contraseña
    if (!data.password) {
        res.code = 401;
        res.msg = 'Debe indicar la contraseña.';
        return res;
    }

    let userData = data;

    // Validar el rol
    let respProcess = await processRol(data.role);
    if (respProcess.code !== 200) {
        res.code = respProcess.code;
        res.msg = respProcess.msg;
        return res;
    }
    // Asignar llave de rol
    userData.role = respProcess.msg.role;

    respProcess = await processCompany(data.company);
    if (respProcess.code !== 200) {
        res.code = respProcess.code;
        res.msg = respProcess.msg;
        return res;
    }
    // Asignar llave de compañia
    userData.company = respProcess.msg.company;

    // Encriptar contraseña
    let hash = await hashPassword(data.password);
    userData.password = hash;

    const user = new User(userData);
    await user.save()
        .then((newUser) => {
            res.resp = true;
            res.code = 200;
            res.msg = newUser.plain();
        })
        .catch((err) => {
            res.code = 500;
            res.msg = err;
        })
    return res;
}

const updateUser = async (data, id) => {
    let res = { code: 0, resp: false, msg: {} };

    let newUserData = data;
    let respProcess;

    if (data.role) {
        // Validar el rol
        respProcess = await processRol(data.role);
        if (respProcess.code !== 200) {
            res.code = respProcess.code;
            res.msg = respProcess.msg;
            return res;
        }
        // Asignar llave de rol
        newUserData.role = respProcess.msg.role;
    }

    if (data.company) {
        respProcess = await processCompany(data.company);
        if (respProcess.code !== 200) {
            res.code = respProcess.code;
            res.msg = respProcess.msg;
            return res;
        }
        // Asignar llave de compañia
        newUserData.company = respProcess.msg.company;
    }

    // Si se recibe una contraseña se encripta
    if (data.password) {
        let hash = await hashPassword(data.password);
        newUserData.password = hash;
    }

    let userId = Number(id);
    // Actualizar información del usuario
    await User.update(userId, newUserData)
        .then((updatedUser) => {
            res.resp = true;
            res.code = 200;
            res.msg = updatedUser.plain();
        })
        .catch((err) => {
            res.code = 500;
            res.msg = err;
        })
    return res;
}

const findByEmail = async (email) => {
    let res;
    // Buscar si existe un usuario asociado al correo electronico
    await User.list({ filters: [['email', email]] })
        .then((entityUser) => {
            res = entityUser.entities;
        })
        .catch(err => { res = err })
    return res;
}

module.exports = {
    getLogin,
    createUser,
    updateUser,
    findByEmail
}
