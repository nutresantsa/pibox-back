/**
 * @version 1.0.0
 * @author Santiago Gonzalez Acevedo <santiago.gonzalez@netwconsulting.com>
 * @copyright 2020 Todos los derechos reservados.
 */

/**
* @controller Servicio de Storage
* @description Script principal para el manejo de archivos
*/

// Cargar configuración de api
const env = require('../lib/setupEnv');

// Manejo de File System y compresion de archivos
const fs = require('fs');
// Tratamiento de imagen
const Jimp = require('jimp');
const stream = require('stream');
const archiver = require('archiver');
// Tratamiento archivo excel
const excel = require('excel4node');
// Encriptación
const uniqid = require('uniqid');
const crypto = require('crypto');
// Configuración
const config = require('../config/config');
// Servicio común
const commonService = require('./common.service');
// Modelo de imagen
const Image = require('../models/image.model');

// Conexión con Google
const gcp = require('../lib/connectGCP');

// Cliente Storage
const bucketName = env.getBucketName();
const storage = gcp.conectionStorage();
// Conexión al bucket
const bucket = storage.bucket(bucketName);



/**
 * @function downloadFile
 * @description Permite subir un archivo de imagen en base64
 */
const downloadFile = async (fileName) => {
    let res = {};
    try {
        let pathFile = `./api/temp/${fileName}`;
        let data = fs.readFileSync(pathFile); // Leer data del archivo excel
        fs.unlinkSync(pathFile); // Eliminar archivo permanentemente
        res = { resp: true, msg: data.toString('base64') };
        return res;
    } catch (error) {
        res = { resp: false, msg: 'El archivo no existe', error };
        return res;
    }
}

/**
 * @function generateZip
 * @description Crea un archivo zip con las imgánes que descarga de Storage
 */
async function generateZip(all_file_links) {
    let res;
    // Generar nombre único para el Zip
    let fileName = uniqid() + crypto.randomBytes(4).toString('hex') + '.zip';
    // Nombre archivo ZIP de salida
    let output = fs.createWriteStream('./api/temp/' + fileName);
    let archive = archiver('zip', {
        gzip: true,
        zlib: { level: 9 } // Establece el nivel de compresión.
    });

    archive.on('error', function (err) {
        throw err;
    });

    // Configuración de salida de archivo ZIP
    archive.pipe(output);

    let cont = 0;

    let te = new Promise(function (resolve, reject) {
        for (let i in all_file_links) {
            let name = all_file_links[i]['name'];
            let remoteFile = bucket.file(name);

            let read_file = remoteFile.createReadStream();

            archive.append(read_file, { name });

            read_file
                .on('error', function (err) {
                    console.log(err);
                    res = { resp: false, msg: err };
                    reject(err);
                })
                .on('response', function (response) {
                    console.log("Escribiendo imagen", name);
                    cont++;
                })
                .on('end', function () {
                    console.log("Archivo descargado", name);
                    if (cont === all_file_links.length) {
                        res = { resp: true, msg: fileName };
                        resolve();
                    }
                })
        }
    })
    await te;
    archive.finalize();
    return res;
}

/**
 * @function downloadAllFolder
 * @description Consulta todos los archivos de una carpeta y los listas para generar zip
 */
const downloadAllFolder = async (ruta) => {
    let res = {};
    try {
        ruta = ruta.toLowerCase();
        const prefix = ruta + "/";
        const delimiter = '/';
        const options = { prefix, delimiter };

        async function listar(options, callback) {
            // Obtiene la lista de los archivos en la ruta especifica
            const [files] = await bucket.getFiles(options);
            if (files.length === 0) {
                res = { resp: false, msg: 'No existen archivos en la ruta.' };
                return res;
            }
            res = await callback(files); // Retorna todos los archivos.
            return res;
        }

        // Se invocan las funciones principales para consulta de archivos y compresion de archivos
        await listar(options, async function (all_file_links) {
            res = await generateZip(all_file_links);
            return res;
        });
        return res;
    } catch (error) {
        res = { resp: false, msg: error };
        return res;
    }
}

/**
 * @function imgConvert
 * @description Permite cambiar tipo y dimensiones una imagen
 */
const imgConvert = async (filename, type, width, higth, route) => {
    let res = { code: 400, msg: '' };
    try {
        let img = new Promise(function (resolve, reject) {

            let ruta = './api/temp/' + filename;
            let routeFile = route + "/" + filename;

            async function saveFile(ruta, routeFile, callback) {
                try {
                    const srcFilename = routeFile;
                    const destFilename = ruta;
                    const options = { destination: destFilename }; // La ruta a la que se debe descargar el archivo
                    // Downloads the file
                    callback(await bucket.file(srcFilename).download(options));
                } catch (error) {
                    res.msg = { resp: false, msg: error.message };
                    reject(error);
                }
            }

            saveFile(ruta, routeFile, function () {
                Jimp.read(ruta, (err, img) => {
                    if (err) throw reject(err);
                    img
                        .resize(width, higth) // Cambiar tamaño imagen
                        .quality(60)
                        .getBuffer(type, (err, buffer) => { //Realiza conversion de la imagen
                            //Transferir imagen buffer a base64 string
                            let base64Image = buffer.toString('base64');
                            let imgSrcString = "data:" + type + ';base64, ' + base64Image;
                            fs.unlinkSync(ruta); //Eliminar imagen subida. Ya tenemos el binario con el cual trabajar.
                            res.code = 200;
                            res.msg = { resp: true, msg: { type, binary: imgSrcString } };
                            resolve();
                        });
                });
            });
        })
        await img;

        return res;
    } catch (error) {
        console.log(error);
        res.msg = { resp: false, msg: error.message };
        return res;
    }
}

/**
 * @function deleteFile
 * @description Permite eliminar un archivo de imagen
 */
async function deleteFile(filePathName) {
    let res = { resp: false, msg: '' };
    try {
        await bucket.file(filePathName).delete();
        res = { resp: true, msg: 'Imagen eliminada' };
        return res;
    } catch (error) {
        res = { msg: error };
        return res;
    }
}

/**
 * @function deleteAndBackup
 * @description Permite crear backup y eliminar imágenes
 */
const deleteAndBackup = async (listImages) => {
    let res = { code: 400, msg: '' };
    try {
        // Genear archivp zip
        let response = await generateZip(listImages);
        // Validar si se genero el archivo zip
        if (!response.resp) {
            res.msg = { resp: false, msg: response.msg };
            return res;
        }
        // Guardar nombre del archivo zip creado
        let fileName = response.msg;

        // Eliminar imagen de Storage y Datastore
        let sw, badFilePathName, badImageID;
        for (let i = 0; i < listImages.length; i++) {
            let filePathName = listImages[i].name;
            let imageID = listImages[i].id;

            // Borrar imagen de Storage
            let response = await deleteFile(filePathName);
            if (!response.resp) {
                sw = false;
                badFilePathName = filePathName;
                break
            }
            // Borrar registros de Datastore
            response = await commonService.deleteModel(Image, imageID);
            if (!response.resp) {
                sw = false;
                badImageID = imageID;
                break
            }
            sw = true;
        }

        // Validar si se elimino correctamente
        if (!sw) {
            res.msg = `Fallo al eliminar imagen: ${badFilePathName} con ID: ${badImageID}`;
            return res;
        }

        res.code = 200;
        res.msg = { resp: true, msg: fileName };
        return res;

    } catch (error) {
        console.log(error);
        res.code = 500;
        res.msg = { resp: false, msg: error.message };
        return res;
    }
}



/**************************
 * FUNCIONES SUBIR ARCHIVO  *
 **************************/

/**
* @function renameFile
* @description Permite renombrar un archivo de Storage
*/
async function renameFile(srcFilename, destFilename) {
    let res = { resp: false, msg: '' };
    try {
        // Renombrar imagen
        await bucket.file(srcFilename).move(destFilename);

        // Hacer pública la URL de la imagen
        const file = bucket.file(destFilename);
        await file.makePublic();

        res = { resp: true, msg: 'Imagen renombrada.' };
        return res;
    } catch (error) {
        res.msg = { msg: error };
        return res;
    }
}

/**
 * @function findImagesByRef
 * @description Buscar registros de imágenes por referencia
 */
async function findImagesByRef(ref) {
    let res = { resp: false, msg: 'No existe la imagen.' };
    // Validar si existe la imagen
    await Image
        .list({ filters: [['ref', ref]] })
        .then(async (entities) => {
            if (entities.entities.length > 0) {
                res = { resp: true, msg: entities.entities };
            }
        })
        .catch(err => { res.msg = err });
    return res;
}

/**
 * @function saveInfoImage
 * @description  Guardar datos de la ruta y version de la imagen en Datastore
 */
async function saveInfoImage(routeFileStorage, version) {
    let res = {};

    let url = `${config.apiStorage}/${bucketName}/${routeFileStorage}`;
    let route = routeFileStorage.split('/');
    let ref = route[4].split('.')[0];

    let newImage = {
        url,
        category: route[0],
        line: route[1],
        brand: route[2],
        gtin: route[3],
        name: route[4],
        ref,
        version
    };
    res = await commonService.createModel(Image, newImage);
    return res;
}

/**
 * @function subirArchivoBase64
 * @description Permite subir un archivo de imagen en base64
 */
async function subirArchivoBase64(image, type, nameFile, routeFile) {
    let res = { resp: false, code: 400, msg: '' };
    try {
        //Armamos las propiedades del archivo base64
        let bufferStream = new stream.PassThrough();
        let fileFullName = nameFile + "." + type;
        bufferStream.end(Buffer.from(image, 'base64'));

        // Definir el nombre del archivo
        let file = bucket.file(routeFile + "/" + fileFullName);
        // Metadata del archivo
        let configMetadata = {
            metadata: {
                contentType: 'image/' + type,
                metadata: {
                    custom: 'metadata'
                }
            },
            public: true, // Validacion para hacer pública la URL de la imagen
            resumable: true,
            validation: 'crc32c'
        };
        let write = new Promise(function (resolve, reject) {
            bufferStream.pipe(file.createWriteStream(configMetadata))
                .on('error', async function (err) {
                    reject(err);
                })
                .on('finish', async function () {
                    let routeFileStorage = file.name;
                    res.resp = true;
                    res.msg = routeFileStorage;
                    resolve();
                });
        })
        await write;
        return res;
    } catch (error) {
        console.log(error);
        res.msg = error.message;
        return res;
    }
}

/**
 * @function saveImage
 * @description Permite guardar una imagen y su información
 */
async function saveImage(image, type, nameFile, routeFile, version) {
    let res = { resp: false, msg: '' };

    let response = await subirArchivoBase64(image, type, nameFile, routeFile);

    // Valida si subio la nueva imagen
    if (!response.resp) {
        res.msg = { resp: false, msg: response.msg };
        return res;
    }
    let routeFileStorage = response.msg;
    let resSave = await saveInfoImage(routeFileStorage, version);

    // Validar si se guarda el registro de la imagen
    if (!resSave.resp) {
        res.msg = { resp: false, msg: resSave.msg };
        return res;
    }
    res.msg = { resp: true, msg: resSave.msg };
    res.resp = true;
    return res;
}

/**
 * @function versionImage
 * @description Subir una nueva versión de imagen
 */
async function versionImage(image, type, nameFile, routeFile, lastImage) {
    let response = {};

    // Obtener datos imagen con mayor versión
    let imageId = Number(lastImage.id);
    let name = lastImage.name;
    let lastVersion = lastImage.version;
    let newVersion = lastVersion + 1;

    // Renombrar imagen principal
    let newName = `${nameFile}-${lastVersion}.${type}`;
    let srcFilename = `${routeFile}/${name}.${type}`;
    let destFilename = `${routeFile}/${newName}`;

    response = await renameFile(srcFilename, destFilename);

    // Validar si fallo al renombrar
    if (!response.resp) {
        return response;
    }

    // Actualizar registro imagen renombrada
    let newUrl = `${config.apiStorage}/${bucketName}/${destFilename}`;
    let imageData = { url: newUrl, name: newName };
    response = await commonService.updateModel(Image, imageData, imageId);

    // Validar si fallo al actualizar registro
    if (!response.resp) {
        res.msg = response;
        return res;
    }

    // Subir nueva imagen
    response = saveImage(image, type, nameFile, routeFile, newVersion);
    return response;
}

/**
 * @function determinateLastImage
 * @description Determina la imagen con la mayor versión
 */
function determinateLastImage(imagesList) {
    let max = 0;
    let lastImage = {};
    imagesList.forEach(element => {
        if (element.version > max) {
            max = element.version;
            const name = element.name.split('.')[0];
            lastImage.version = max;
            lastImage.name = name;
            lastImage.id = element.id;
        }
    });
    return lastImage;
}

/**
 * @function upload
 * @description Subir imagen nueva o versionada
 */
const upload = async (image, type, nameFile, routeFile) => {
    let res = { code: 400, msg: '' };

    // Nombre de la imagen a subir
    const ref = nameFile;

    // Buscar si existe la imagen
    let response = await findImagesByRef(ref);

    if (!response.resp) {
        // Si es false se subira una nueva imagen
        const version = 1;
        response = await saveImage(image, type, nameFile, routeFile, version);
    } else {
        // Generar nueva version a partir de la información de la ultima imagen principal
        let imagesList = response.msg;
        const lastImage = determinateLastImage(imagesList);
        response = await versionImage(image, type, nameFile, routeFile, lastImage);
    }

    if (response.resp) {
        res.code = 200;
    }
    res.msg = response.msg;
    return res;
}

/**************************
 * fin FUNCIONES SUBIR ARCHIVO  *
 **************************/




/****************************
 * FUNCIONES GENERAR REPORTE *
 ****************************/

// Asigna formato de d/m/y a una fecha
function getDate(d) {
    let now = d;
    let dd = now.getDate();
    let mm = now.getMonth() + 1;
    let yyyy = now.getFullYear();

    dd = addZero(dd);
    mm = addZero(mm);

    // Formato para archivo excel
    now = dd + '/' + mm + '/' + yyyy;
    return now;

    function addZero(i) {
        if (i < 10) i = '0' + i;
        return i;
    }
}

/**
 * @function createReport
 * @description Crea un archivo excel de 2 columnas y n filas
 */
async function createReport(images) {
    let res = { resp: false, msg: '' };
    try {
        // Crear instancia de la clase Workbook
        let workbook = new excel.Workbook();
        // Agregar hoja al archivo excel
        let worksheet = workbook.addWorksheet('PiBox');
        // Crear estilo para cabecera
        let style = workbook.createStyle({
            font: { bold: true, size: 14 }
        });
        let row = 1;
        worksheet.cell(row, 1).string('Nombre').style(style); // Titulo de la columna de nombres
        worksheet.cell(row, 2).string('Fecha de carga').style(style); // Titulo de la columna de fechas
        worksheet.cell(row, 3).string('Link público').style(style); // Titulo de la columna de fechas

        // Ordenar fecha de mayor a menor
        images.sort(function (a, b) {
            return (b.dateUpload - a.dateUpload)
        })
        // Escribir nombre y fecha de cada imagen por fila
        images.forEach(element => {
            row++;
            worksheet.cell(row, 1).string(element.name);
            worksheet.cell(row, 2).string(getDate(element.dateUpload));
            worksheet.cell(row, 3).string(element.url);
        });
        // Nombre único del archivo
        let fileName = uniqid() + crypto.randomBytes(4).toString('hex') + '.xlsx';
        // Ruta para guardar el archivo
        let pathFile = `./api/temp/${fileName}`;
        workbook.write(pathFile);
        res = { resp: true, msg: fileName };
        return res;
    } catch (error) {
        res = { resp: false, msg: 'Error al generar reporte', error };
        return res;
    }
}

/**
 * @function generateReport
 * @description Permite consultar el listado de imágenes paara generar reporte
 */
const generateReport = async () => {
    let res = { resp: false, msg: '' };
    await Image.list()
        .populate()
        .then(async (entities) => {
            res = await createReport(entities.entities);
        })
        .catch(err => { res.msg = err });
    return res;
}
/****************************
 * fin FUNCIONES GENERAR REPORTE *
 ****************************/


module.exports = {
    upload,
    generateReport,
    downloadAllFolder,
    imgConvert,
    downloadFile,
    deleteAndBackup
}
