/**
 * @version 1.0.0
 * @author Santiago Gonzalez Acevedo <santiago.gonzalez@netwconsulting.com>
 * @copyright 2020 Todos los derechos reservados.
 */

/**
* @controller Controlador de rutas API para las funcionalidades de usuarios
* @description Permite configurar las rutas de los servicios para la API de usuario y su gestion
*/
const express = require('express');
const router = express.Router();

// Se requiere el controlador del usuario donde se encuentran todas las funcionalidades de usuarios de la API
const category_controller = require('../controllers/category.controller');
const line_controller = require('../controllers/line.controller');
const brand_controller = require('../controllers/brand.controller');
const company_controller = require('../controllers/company.controller');



/**************************
 * RUTAS DE CATEGORÍA    *
 **************************/
router.post('/createCategory', category_controller.createCategory);
router.get('/getCategory', category_controller.getCategory);
router.get('/getCategories', category_controller.getCategories);
router.get('/getActiveCategories', category_controller.getActiveCategories);
router.put('/updateCategory', category_controller.updateCategory);
router.delete('/deleteCategory', category_controller.deleteCategory);


/**************************
 * RUTAS DE LÍNEA *
 **************************/
router.post('/createLine', line_controller.createLine);
router.get('/getLine', line_controller.getLine);
router.get('/getLines', line_controller.getLines);
router.get('/getActiveLines', line_controller.getActiveLines);
router.put('/updateLine', line_controller.updateLine);
router.delete('/deleteLine', line_controller.deleteLine);


/**************************
 * RUTAS DE MARCA   *
 **************************/
router.post('/createBrand', brand_controller.createBrand);
router.get('/getBrand', brand_controller.getBrand);
router.get('/getBrands', brand_controller.getBrands);
router.get('/getActiveBrands', brand_controller.getActiveBrands);
router.put('/updateBrand', brand_controller.updateBrand);
router.delete('/deleteBrand', brand_controller.deleteBrand);


/**************************
 * RUTAS DE COMPAÑIA *
 **************************/
router.get('/getCompany', company_controller.getCompany);
router.get('/getCompanies', company_controller.getCompanies);
router.post('/createCompany', company_controller.createCompany);
router.delete('/deleteCompany', company_controller.deleteCompany);


module.exports = router;
