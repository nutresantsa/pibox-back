/**
 * @version 1.0.0
 * @author Santiago Gonzalez Acevedo <santiago.gonzalez@netwconsulting.com>
 * @copyright 2020 Todos los derechos reservados.
 */

/**
* @controller Controlador de rutas API para las funcionalidades de Google Cloud Storage
* @description Permite configurar las rutas de los servicios para la API de usuario y su gestion
*/
const express = require('express');
const router = express.Router();

// Se requiere el controlador del storage donde se encuentran todas las funcionalidades de Storage de la API
const storage_controller = require('../controllers/storage.controller');

router.post('/upload', storage_controller.upload);
router.get('/downloadAllFolder', storage_controller.downloadAllFolder);
router.get('/generateReport', storage_controller.generateReport);
router.get('/downloadFile', storage_controller.downloadFile);
router.post('/imgConvert', storage_controller.imgConvert);
router.post('/findImagesWithFilter', storage_controller.findImagesWithFilter);
router.post('/deleteImages', storage_controller.deleteImages);

module.exports = router;
