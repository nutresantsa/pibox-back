/**
 * @version 1.0.0
 * @author Sebastian Cardona Loaiza <sebastian.cardona@gruponetw.com>
 * @copyright 2020 Todos los derechos reservados.
 */
/**
* @controller Controlador de marcas
* @description Script NODEJS que permite realizar operaciones sobre las marcas registrados.
*/

/**************************
 * INCIO DEPENDENCIAS     *
 **************************/
// Modelo
const Brand = require('../models/brand.model');
// Servicio
const commonService = require('../service/common.service');
// Autenticación JWT
const auth = require('../auth/securityJWT');
/**************************
 * FIN DEPENDENCIAS       *
 **************************/

/**
 * @function getBrands
 * @param {Request} req Obtener parametros de cabecera
 * @param {Response} res Obtener valores del Body
 * @description Permite listar todas las marcas
 */
const getBrands = async (req, res) => {
    try {
        // Validar el token
        let resToken = auth.verifyToken(req);
        if (!resToken.resp) return res.status(401).send(resToken);
        const brandList = await commonService.getModels(Brand);
        res.status(200).send(brandList);
    } catch (error) {
        console.log(error.message);
        res.status(500).send(error.message);
    }
};
/**
 * @function getActiveBrands
 * @param {Request} req Obtener parametros de cabecera
 * @param {Response} res Obtener valores del Body
 * @description Permite obtener la lista de marcas activas
 */
const getActiveBrands = async (req, res) => {
    try {
        // Validar el token
        let resToken = auth.verifyToken(req);
        if (!resToken.resp) return res.status(401).send(resToken);
        const brandList = await commonService.getActiveModel(Brand);
        res.status(200).send(brandList);
    } catch (error) {
        console.log(error.message);
        res.status(500).send(error.message);
    }
};

/**
 * @function getBrand
 * @param {Request} req Obtener parametros de cabecera
 * @param {Response} res Obtener valores del Body
 * @description Permite obtener una marca filtrado por ID.
 */
const getBrand = async (req, res) => {
    try {
        // Validar el token
        let resToken = auth.verifyToken(req);
        if (!resToken.resp) return res.status(401).send(resToken);
        let id = req.headers['id'];
        const response = await commonService.getModel(Brand, id);
        if (response.resp === false) {
            res.status(400).send(response);
        }
        res.status(200).send(response);
    } catch (error) {
        console.log(error.message);
        res.status(500).send(error.message);
    }
};


/**
 * @function createBrand
 * @param {Request} req Obtener parametros de cabecera
 * @param {Response} res Obtener valores del Body
 * @description Permite crear una marca nueva en el DataStore
 */
const createBrand = async (req, res) => {
    try {
        // Validar el token
        let resToken = auth.verifyToken(req);
        if (!resToken.resp) return res.status(401).send(resToken);
        const data = Brand.sanitize(req.body);
        const brand = await commonService.createModel(Brand, data);
        res.status(200).send(brand);
    } catch (error) {
        console.log(error.message);
        res.status(500).send(error.message);
    }
};

/**
 * @function updateBrand
 * @param {Request} req Obtener parametros de cabecera
 * @param {Response} res Obtener valores del Body
 * @description Permite actualizar una marca especifica por ID
 */
const updateBrand = async (req, res) => {
    try {
        // Validar el token
        let resToken = auth.verifyToken(req);
        if (!resToken.resp) return res.status(401).send(resToken);
        let id = req.headers['id'];
        const data = Brand.sanitize(req.body);
        const brand = await commonService.updateModel(Brand, data, id);
        res.status(200).send(brand);
    } catch (error) {
        console.log(error.message);
        res.status(500).send(error.message);
    }
};

/**
 * @function deleteBrand
 * @param {Request} req Obtener parametros de cabecera
 * @param {Response} res Obtener valores del Body
 * @description Permite eliminar un marca especifica por ID
 */
const deleteBrand = async (req, res) => {
    try {
        // Validar el token
        let resToken = auth.verifyToken(req);
        if (!resToken.resp) return res.status(401).send(resToken);
        let id = req.headers['id'];
        const resp = await commonService.deleteModel(Brand, id);
        res.status(200).send(resp);
    } catch (error) {
        console.log(error.message);
        res.status(500).send(error.message);
    }
};

//Exportar funciones
module.exports = {
    getBrands,
    getActiveBrands,
    getBrand,
    createBrand,
    updateBrand,
    deleteBrand
};
