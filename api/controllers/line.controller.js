/**
 * @version 1.0.0
 * @author Sebastian Cardona Loaiza <sebastian.cardona@gruponetw.com>
 * @copyright 2020 Todos los derechos reservados.
 */

/**
* @controller Controlador de líneas
* @description Script NODEJS que permite realizar operaciones sobre las líneas registrados.
*/

/**************************
 * INCIO DEPENDENCIAS     *
 **************************/
// Modelo
const Line = require('../models/line.model');
// Servicio
const commonService = require('../service/common.service');
// Autenticación JWT
const auth = require('../auth/securityJWT');
/**************************
 * FIN DEPENDENCIAS       *
 **************************/

/**
 * @function getLines
 * @param {Request} req Obtener parametros de cabecera
 * @param {Response} res Obtener valores del Body
 * @description Permite listar todas las líneas
 */
const getLines = async (req, res) => {
    try {
        // Validar el token
        let resToken = auth.verifyToken(req);
        if (!resToken.resp) return res.status(401).send(resToken);
        const lineList = await commonService.getModels(Line);
        res.status(200).send(lineList);
    } catch (error) {
        console.log(error.message);
        res.status(500).send(error.message);
    }
};
/**
 * @function getActiveLines
 * @param {Request} req Obtener parametros de cabecera
 * @param {Response} res Obtener valores del Body
 * @description Permite obtener la lista de líneas activas
 */
const getActiveLines = async (req, res) => {
    try {
        // Validar el token
        let resToken = auth.verifyToken(req);
        if (!resToken.resp) return res.status(401).send(resToken);
        const lineList = await commonService.getActiveModel(Line);
        res.status(200).send(lineList);
    } catch (error) {
        console.log(error.message);
        res.status(500).send(error.message);
    }
};

/**
 * @function getLine
 * @param {Request} req Obtener parametros de cabecera
 * @param {Response} res Obtener valores del Body
 * @description Permite obtener una linea filtrado por ID.
 */
const getLine = async (req, res) => {
    try {
        // Validar el token
        let resToken = auth.verifyToken(req);
        if (!resToken.resp) return res.status(401).send(resToken);
        let id = req.headers['id'];
        const response = await commonService.getModel(Line, id);
        if (response.resp === false) {
            res.status(400).send(response);
        }
        res.status(200).send(response);
    } catch (error) {
        console.log(error.message);
        res.status(500).send(error.message);
    }
};


/**
 * @function createLine
 * @param {Request} req Obtener parametros de cabecera
 * @param {Response} res Obtener valores del Body
 * @description Permite crear una linea nueva en el DataStore
 */
const createLine = async (req, res) => {
    try {
        // Validar el token
        let resToken = auth.verifyToken(req);
        if (!resToken.resp) return res.status(401).send(resToken);
        const data = Line.sanitize(req.body);
        const line = await commonService.createModel(Line, data);
        res.status(200).send(line);
    } catch (error) {
        console.log(error.message);
        res.status(500).send(error.message);
    }
};

/**
 * @function updateLine
 * @param {Request} req Obtener parametros de cabecera
 * @param {Response} res Obtener valores del Body
 * @description Permite actualizar una linea especifica por ID
 */
const updateLine = async (req, res) => {
    try {
        // Validar el token
        let resToken = auth.verifyToken(req);
        if (!resToken.resp) return res.status(401).send(resToken);
        let id = req.headers['id'];
        const data = Line.sanitize(req.body);
        const line = await commonService.updateModel(Line, data, id);
        res.status(200).send(line);
    } catch (error) {
        console.log(error.message);
        res.status(500).send(error.message);
    }
};

/**
 * @function deleteLine
 * @param {Request} req Obtener parametros de cabecera
 * @param {Response} res Obtener valores del Body
 * @description Permite eliminar un linea especifica por ID
 */
const deleteLine = async (req, res) => {
    try {
        // Validar el token
        let resToken = auth.verifyToken(req);
        if (!resToken.resp) return res.status(401).send(resToken);
        let id = req.headers['id'];
        const resp = await commonService.deleteModel(Line, id);
        res.status(200).send(resp);
    } catch (error) {
        console.log(error.message);
        res.status(500).send(error.message);
    }
};

//Exportar funciones
module.exports = {
    getLines,
    getActiveLines,
    getLine,
    createLine,
    updateLine,
    deleteLine
};
