/**
 * @version 1.0.0
 * @author Santiago Gonzalez Acevedo <santiago.gonzalez@netwconsulting.com>
 * @copyright 2020 Todos los derechos reservados.
 */

/**
* @controller Controlador archivos y almacenamiento
* @description Script principal para el manejo de archivos
*/

// Autenticación JWT
const auth = require('../auth/securityJWT');
// Modelo de imagen
const Image = require('../models/image.model');

// Servicio
const storageService = require('../service/storage.service');
const commonService = require('../service/common.service');

// Script de validaciones
const validateData = require('../tools/validations/validateData');


/**
 * @function upload
 * @param {Request} req Obtener parametros de cabecera
 * @param {Response} res Obtener valores del Body
 * @description Permite subir un archivo de imagen en base64
 */
const upload = async (req, res) => {
    let resp = { code: 400, msg: '' };
    try {
        // Validar el token
        let resToken = auth.verifyToken(req);
        if (!resToken.resp) return res.status(401).send(resToken);

        // Obtener parametros del Body
        let image = req.body.imageBase64;
        let type = req.body.type;
        let nameFile = req.body.nameFile;
        let routeFile = req.body.routeFile;

        // Validar si se enviaron todos los datos necesarios
        if (validateData.isEmpty(image) || validateData.isEmpty(type) || validateData.isEmpty(nameFile) || validateData.isEmpty(routeFile)) {
            res.status(400).send({ resp: false, msg: "Faltan datos en la solicitud." });
            return;
        }
        // Validar si es de tipo png
        if (!validateData.validateImageType(type)) {
            res.status(400).send({ resp: false, msg: "Tipo de archivo incorrecto." });
            return;
        }
        // Validar el nombre de la imagen según estandar
        if (!validateData.validateImageName(nameFile)) {
            res.status(400).send({ resp: false, msg: "Formato de nombre de archivo incorrecto." });
            return;
        }
        // Validar ruta de la imagen
        let validateRoute = validateData.validateRoute(nameFile, routeFile);
        if (!validateRoute.resp) {
            res.status(400).send({ resp: false, msg: validateRoute.msg });
            return;
        }
        routeFile = routeFile.toLowerCase();
        // Buscar imágenes
        resp = await storageService.upload(image, type, nameFile, routeFile);
        res.status(resp.code).send(resp.msg);
    } catch (error) {
        console.log(error.message);
        res.status(500).send(error.message);
    }
}

/**
 * @function downloadAllFolder
 * @param {Request} req Obtener parametros de cabecera
 * @param {Response} res Obtener valores del Body
 * @description Permite generartodas las imágenes de una ruta en formato zip
 */
const downloadAllFolder = async (req, res) => {
    try {
        // Validar el token
        let resToken = auth.verifyToken(req);
        if (!resToken.resp) return res.status(401).send(resToken);
        // Se envia desde la invocacion del servicio la ruta de las carpetas
        let ruta = req.headers['ruta'];
        // Validar si se enviaron todos los datos necesarios
        if (validateData.isEmpty(ruta)) {
            res.status(400).send({ resp: false, msg: "Debe definir una ruta." });
            return;
        }
        let resp = await storageService.downloadAllFolder(ruta);
        res.status(200).send(resp);
    } catch (error) {
        console.log(error.message);
        res.status(500).send(error.message);
    }
}

/**
 * @function imgConvert
 * @param {Request} req Obtener parametros de cabecera
 * @param {Response} res Obtener valores del Body
 * @description Permite convertir el formato de y dimensiones de imagen
 */
const imgConvert = async (req, res) => {
    try {
        // Validar el token
        let resToken = auth.verifyToken(req);
        if (!resToken.resp) return res.status(401).send(resToken);

        let filename = req.body.filename;
        let route = req.body.route;
        let type = req.body.type;
        let width = req.body.width;
        let higth = req.body.higth;

        // Validar si se enviaron todos los datos necesarios
        if (validateData.isEmpty(filename) || validateData.isEmpty(route) || validateData.isEmpty(type) || validateData.isEmpty(width) || validateData.isEmpty(higth)) {
            return res.status(400).send({ resp: false, msg: "Faltan campos en la solicitud." });
        }
        if (type !== 'image/png' && type !== 'image/jpeg') {
            return res.status(400).send({ resp: false, msg: "Solo se permite convertir en PNG o JPEG." });
        }
        if (!validateData.expNumber(width) || !validateData.expNumber(higth)) {
            return res.status(400).send({ resp: false, msg: "El ancho y alto deben ser números." });
        }
        width = Number(width);
        higth = Number(higth);
        if (width > 2000 || higth > 2000) {
            return res.status(400).send({ resp: false, msg: "El maximo tamaño es 2000x2000" });
        }
        // Convertir imagen
        const response = await storageService.imgConvert(filename, type, width, higth, route);
        res.status(response.code).send(response.msg);
    } catch (error) {
        console.log(error.message);
        res.status(500).send(error.message);
    }
}

/**
 * @function findImagesWithFilter
 * @param {Request} req Obtener parametros de cabecera
 * @param {Response} res Obtener valores del Body
 * @description Busca los registro de imágenes por filtro
 */
const findImagesWithFilter = async (req, res) => {
    try {
        // Validar el token
        let resToken = auth.verifyToken(req);
        if (!resToken.resp) return res.status(401).send(resToken);
        let options = req.body;
        let filter = { filters: [] };
        // Valida si se filta por categoria
        if (options.category !== undefined && options.category !== "") {
            filter.filters.push(['category', options.category])
        }
        // Valida si se filta por línea
        if (options.line !== undefined && options.line !== "") {
            filter.filters.push(['line', options.line])
        }
        // Valida si se filta por marca
        if (options.brand !== undefined && options.brand !== "") {
            filter.filters.push(['brand', options.brand])
        }
        // Valida si se filta por gtin
        if (options.gtin !== undefined && options.gtin !== "") {
            filter.filters.push(['gtin', options.gtin])
        }
        // Buscar imágenes
        const imageList = await commonService.listModelsWithFilter(Image, filter);
        res.status(200).send(imageList);
    } catch (error) {
        console.log(error.message);
        res.status(500).send(error.message);
    }
}

/**
 * @function generateReport
 * @param {Request} req Obtener parametros de cabecera
 * @param {Response} res Obtener valores del Body
 * @description Genera un reporte en excel con la lista de imágenes en storage
 */
const generateReport = async (req, res) => {
    try {
        // Validar el token
        let resToken = auth.verifyToken(req);
        if (!resToken.resp) return res.status(401).send(resToken);
        let resp = await storageService.generateReport();
        res.status(200).send(resp);
    } catch (error) {
        console.log(error.message);
        res.status(500).send(error.message);
    }
}


/**
 * @function downloadFile
 * @param {Request} req Obtener parametros de cabecera
 * @param {Response} res Obtener valores del Body
 * @description Descarga un archivo de la carpeta temporal, al finalizar se elimina
 */
const downloadFile = async (req, res) => {
    try {
        // Validar el token
        let resToken = auth.verifyToken(req);
        if (!resToken.resp) return res.status(401).send(resToken);
        let fileName = req.headers['filename'];
        // Validar si se enviaron todos los datos necesarios
        if (validateData.isEmpty(fileName)) {
            res.status(400).send({ resp: false, msg: "Debe indicar el nombre del archivo." });
            return;
        }
        let resp = await storageService.downloadFile(fileName);
        res.status(200).send(resp);
    } catch (error) {
        console.log(error.message);
        res.status(500).send(error.message);
    }
}


/**
 * @function deleteFiles
 * @param {Request} req Obtener parametros de cabecera
 * @param {Response} res Obtener valores del Body
 * @description Elimina imágenes de Storage
 */
const deleteImages = async (req, res) => {
    try {
        // Validar el token
        let resToken = auth.verifyToken(req);
        if (!resToken.resp) return res.status(401).send(resToken);

        let listImagesID = req.body['listImagesID'];
        // Validar si se enviaron todos los datos necesarios
        if (validateData.isEmpty(listImagesID)) {
            res.status(400).send({ resp: false, msg: "Debe indicar la lista de imágenes." });
            return;
        }
        if (listImagesID.length === 0) {
            res.status(400).send({ resp: false, msg: "Debe enviar la lista de imágenes." });
            return;
        }

        // Validar si existen los ID
        let sw, badID, listImages = [];
        for (let i = 0; i < listImagesID.length; i++) {
            let imageID = listImagesID[i];

            let response = await commonService.getModel(Image, imageID);
            if (!response.resp) {
                sw = false;
                badID = imageID;
                break
            }
            sw = true;
            // Guardar informacion de la imagen
            let image = {
                id: response.msg.id,
                name: `${response.msg.category}/${response.msg.line}/${response.msg.brand}/${response.msg.gtin}/${response.msg.name}`
            }
            listImages.push(image);
        }
        if (!sw) {
            res.status(400).send({ resp: false, msg: `No existe imagen con id: ${badID}` });
            return;
        }

        let response = await storageService.deleteAndBackup(listImages);
        res.status(response.code).send(response.msg);
    } catch (error) {
        console.log(error.message);
        res.status(500).send(error.message);
    }
}

// Exportar funciones
module.exports = {
    upload,
    downloadAllFolder,
    generateReport,
    downloadFile,
    imgConvert,
    findImagesWithFilter,
    deleteImages
};
