/**
 * @version 1.0.0
 * @author Sebastian Cardona Loaiza <sebastian.cardona@gruponetw.com>
 * @copyright 2020 Todos los derechos reservados.
 */

/**
* @controller Controlador de compañias
* @description Script NODEJS que permite realizar operaciones sobre las compañias registrados.
*/

/**************************
 * INCIO DEPENDENCIAS     *
 **************************/
// Modelo
const Company = require('../models/company.model');
// Servicio
const commonService = require('../service/common.service');
// Autenticación JWT
const auth = require('../auth/securityJWT');
/**************************
 * FIN DEPENDENCIAS       *
 **************************/

/**
 * @function getCompanies
 * @param {Request} req Obtener parametros de cabecera
 * @param {Response} res Obtener valores del Body
 * @description Permite listar todas las compañias
 */
const getCompanies = async (req, res) => {
    try {
        // Validar el token
        let resToken = auth.verifyToken(req);
        if (!resToken.resp) return res.status(401).send(resToken);
        const companyList = await commonService.getModels(Company);
        res.status(200).send(companyList);
    } catch (error) {
        console.log(error.message);
        res.status(500).send(error.message);
    }
};

/**
 * @function getCompany
 * @param {Request} req Obtener parametros de cabecera
 * @param {Response} res Obtener valores del Body
 * @description Permite obtener una compañia filtrada por ID.
 */
const getCompany = async (req, res) => {
    try {
        // Validar el token
        let resToken = auth.verifyToken(req);
        if (!resToken.resp) return res.status(401).send(resToken);
        let id = req.headers['id'];
        const response = await commonService.getModel(Company, id);
        if (response.resp === false) {
            res.status(400).send(response);
        }
        res.status(200).send(response);
    } catch (error) {
        console.log(error.message);
        res.status(500).send(error.message);
    }
};

/**
 * @function createCompany
 * @param {Request} req Obtener parametros de cabecera
 * @param {Response} res Obtener valores del Body
 * @description Permite crear una compañia nueva en el DataStore
 */
const createCompany = async (req, res) => {
    try {
        // Validar el token
        let resToken = auth.verifyToken(req);
        if (!resToken.resp) return res.status(401).send(resToken);
        const data = Company.sanitize(req.body);
        const company = await commonService.createModel(Company, data);
        res.status(200).send(company);
    } catch (error) {
        console.log(error.message);
        res.status(500).send(error.message);
    }
};

/**
 * @function deleteCompany
 * @param {Request} req Obtener parametros de cabecera
 * @param {Response} res Obtener valores del Body
 * @description Permite eliminar un compañia especifica por ID
 */
const deleteCompany = async (req, res) => {
    try {
        // Validar el token
        let resToken = auth.verifyToken(req);
        if (!resToken.resp) return res.status(401).send(resToken);
        let id = req.headers['id'];
        const resp = await commonService.deleteModel(Company, id);
        res.status(200).send(resp);
    } catch (error) {
        console.log(error.message);
        res.status(500).send(error.message);
    }
};

//Exportar funciones
module.exports = {
    getCompanies,
    getCompany,
    createCompany,
    deleteCompany
};
