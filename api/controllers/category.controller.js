/**
 * @version 1.0.0
 * @author Sebastian Cardona Loaiza <sebastian.cardona@gruponetw.com>
 * @copyright 2020 Todos los derechos reservados.
 */

/**
* @controller Controlador de categorías
* @description Script NODEJS que permite realizar operaciones sobre las categorías registrados.
*/

/**************************
 * INCIO DEPENDENCIAS     *
 **************************/
// Modelo
const Category = require('../models/category.model');
// Servicio
const commonService = require('../service/common.service');
// Autenticación JWT
const auth = require('../auth/securityJWT');
/**************************
 * FIN DEPENDENCIAS       *
 **************************/

/**
 * @function getCategories
 * @param {Request} req Obtener parametros de cabecera
 * @param {Response} res Obtener valores del Body
 * @description Permite listar todas las categorías
 */
const getCategories = async (req, res) => {
    try {
        // Validar el token
        let resToken = auth.verifyToken(req);
        if (!resToken.resp) return res.status(401).send(resToken);
        const categoryList = await commonService.getModels(Category);
        res.status(200).send(categoryList);
    } catch (error) {
        console.log(error.message);
        res.status(500).send(error.message);
    }
};
/**
 * @function getActiveCategories
 * @param {Request} req Obtener parametros de cabecera
 * @param {Response} res Obtener valores del Body
 * @description Permite obtener la lista de categorias activas
 */
const getActiveCategories = async (req, res) => {
    try {
        // Validar el token
        let resToken = auth.verifyToken(req);
        if (!resToken.resp) return res.status(401).send(resToken);
        const categoryList = await commonService.getActiveModel(Category);
        res.status(200).send(categoryList);
    } catch (error) {
        console.log(error.message);
        res.status(500).send(error.message);
    }
};

/**
 * @function getCategory
 * @param {Request} req Obtener parametros de cabecera
 * @param {Response} res Obtener valores del Body
 * @description Permite obtener una categoría filtrado por ID.
 */
const getCategory = async (req, res) => {
    try {
        // Validar el token
        let resToken = auth.verifyToken(req);
        if (!resToken.resp) return res.status(401).send(resToken);
        let id = req.headers['id'];
        const response = await commonService.getModel(Category, id);
        if (response.resp === false) {
            res.status(400).send(response);
        }
        res.status(200).send(response);
    } catch (error) {
        console.log(error.message);
        res.status(500).send(error.message);
    }
};


/**
 * @function createCategory
 * @param {Request} req Obtener parametros de cabecera
 * @param {Response} res Obtener valores del Body
 * @description Permite crear una categoría nueva en el DataStore
 */
const createCategory = async (req, res) => {
    try {
        // Validar el token
        let resToken = auth.verifyToken(req);
        if (!resToken.resp) return res.status(401).send(resToken);
        const data = Category.sanitize(req.body);
        const category = await commonService.createModel(Category, data);
        res.status(200).send(category);
    } catch (error) {
        console.log(error.message);
        res.status(500).send(error.message);
    }
};

/**
 * @function updateCategory
 * @param {Request} req Obtener parametros de cabecera
 * @param {Response} res Obtener valores del Body
 * @description Permite actualizar una categoría especifica por ID
 */
const updateCategory = async (req, res) => {
    try {
        // Validar el token
        let resToken = auth.verifyToken(req);
        if (!resToken.resp) return res.status(401).send(resToken);
        let id = req.headers['id'];
        const data = Category.sanitize(req.body);
        const category = await commonService.updateModel(Category, data, id);
        res.status(200).send(category);
    } catch (error) {
        console.log(error.message);
        res.status(500).send(error.message);
    }
};

/**
 * @function deleteCategory
 * @param {Request} req Obtener parametros de cabecera
 * @param {Response} res Obtener valores del Body
 * @description Permite eliminar un categoría especifica por ID
 */
const deleteCategory = async (req, res) => {
    try {
        // Validar el token
        let resToken = auth.verifyToken(req);
        if (!resToken.resp) return res.status(401).send(resToken);
        let id = req.headers['id'];
        const resp = await commonService.deleteModel(Category, id);
        res.status(200).send(resp);
    } catch (error) {
        console.log(error.message);
        res.status(500).send(error.message);
    }
};

//Exportar funciones
module.exports = {
    getCategories,
    getActiveCategories,
    getCategory,
    createCategory,
    updateCategory,
    deleteCategory
};
