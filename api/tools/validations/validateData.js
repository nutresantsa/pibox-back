/**
 * @version 1.0.0
 * @author Sebastian Cardona Loaiza <sebastian.cardona@gruponetw.com>
 * @copyright 2020 Todos los derechos reservados.
 */

/**
* @description Script para declear funciones de validación
*/


/**
 * @function validateImageName
 * @param {String} nameFile Nombre del archivo que se desea subir
 * @description Valida si el nombre del archivo cumple los estandares de nomenclatura
 */
const validateImageName = (nameFile) => {
    let sw = false;
    let infoImage, type, face, angule, status, GTIN;

    // Validar separación por guion bajo
    let separation = nameFile.includes("_");
    if (!separation) {
        return sw;
    }
    GTIN = nameFile.split("_")[0]; // obtener gtin de la imagen
    let regex = /^([0-9])*$/; // expresion regular para solo números
    // Validar que sea solo números
    if (!regex.test(GTIN)) {
        return sw;
    }
    // Validar tamaño del gtin (8 - 13 - 14)
    switch (GTIN.length) {
        case 8:
            break;
        case 13:
            break;
        case 14:
            break;
        default:
            return sw;
    }
    infoImage = nameFile.split("_")[1];
    // Validar si se tienen los 4 caracteres
    if (infoImage.length !== 4) {
        return sw;
    }
    // Validar tipo de imagen
    type = infoImage.charAt(0);
    if (type !== "A" && type !== "B" && type !== "H" && type !== "L") {
        return sw;
    }
    // Validar cara
    face = infoImage.charAt(1);
    if (face !== "1" && face !== "2") {
        return sw;
    }
    // Validar ángulo
    angule = infoImage.charAt(2);
    if (angule !== "C" && angule !== "L" && angule !== "R") {
        return sw;
    }
    // Validar estado
    status = infoImage.charAt(3);
    if (status !== "1" && status !== "0" && status !== "E") {
        return sw;
    }
    return true;
}

/**
 * @function validateImageType
 * @param {String} type Tipo de archivo que se desea subir
 * @description Valida si el tipo del archivo es png
 */
const validateImageType = (type) => {
    return (type === 'png');
}

/**
 * @function validateRoute
 * @param {String} nameFile Nombre de la imagen
 * @param {String} routeFile Ruta de destino en bucket
 * @description Valida si una ruta esta completa y su GTIN es igual al de la imagen
 */
const validateRoute = (nameFile, routeFile) => {
    let res = { resp: false, msg: '' };

    let name = nameFile.split('_');
    let route = routeFile.split('/');

    if (route.length !== 4) {
        res.msg = 'Debe indicar una ruta completa: categoria/linea/marca/gtin';
        return res;
    }
    if (name[0] !== route[3]) {
        res.msg = 'El GTIN de la ruta y del nombre de imagen no coinciden.';
        return res;
    }
    res.resp = true;
    res.msg = 'Bien';
    return res;
}

/**
 * @function isEmpty
 * @param {String} data Dato que se desea validar
 * @description Valida si un dato no es vacio
 */
const isEmpty = (data) => {
    return (data === '' || data === undefined);
}

/**
 * @function expNumber
 * @param {any} numero Dato que se desea validar
 * @description Validar si contiene solo números
 */
function expNumber(numero) {
    var regex = /^([0-9])*$/;
    return regex.test(numero);
}


module.exports = {
    validateImageName,
    validateImageType,
    validateRoute,
    isEmpty,
    expNumber
}
