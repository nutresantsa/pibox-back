const path = require('path');
const cron = require("node-cron");
const fs = require('fs');

function cleanTemp() {
    cron.schedule("0 59 23 * * 7", deleteFiles);
}

async function deleteFiles() {
    const directory = './api/temp';
    let sw = false;
    let i = 0;
    let goToDelete = new Promise(function (resolve, reject) {
        fs.readdir(directory, (err, files) => {
            if (err) throw reject(err);
            for (const file of files) {
                fs.unlink(path.join(directory, file), err => {
                    if (err) throw err;
                });
                i++;
            }
            let date = new Date();
            console.log(`Archivos borrados: ${i} | Fecha: ${date}`);
            resolve();
            sw = true;
        })
    })
    await goToDelete;
    return sw;
}

// Se exporta deleteFiles para prueba unitaria
module.exports = {
    cleanTemp,
    deleteFiles
}
