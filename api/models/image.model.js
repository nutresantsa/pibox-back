/**
 * @version 1.0.0
 * @author Sebastian Cardona Loaiza <sebastian.cardona@gruponetw.com>
 * @copyright 2020 Todos los derechos reservados.
 */

/**
* @controller Modelo de imagen
* @description Se configura el modelo de imagen. Se configura la tabla NoSQL de la base de datos para el esquema.
*/

//Dependencias
const { instances } = require('gstore-node');

// Recupera la instancia para gstore
const gstore = instances.get('unique-id');
const { Schema } = gstore;

/**
 * Creando el esquema para el modelo de imagen
*/
const imageSchema = new Schema({
    url: { type: String, required: true },
    category: { type: String, required: true },
    line: { type: String, required: true },
    brand: { type: String, required: true },
    gtin: { type: String, required: true },
    name: { type: String, required: true },
    ref: { type: String, required: true },
    dateUpload: { type: Date, default: gstore.defaultValues.NOW },
    version: { type: Number, required: true }
});

/**
 * Configuración para ordenar alfabeticamente
 */
const listSettings = {
    order: { property: 'dateUpload' }
};

imageSchema.queries('list');

//Exporto el esquema de base de datos como 'Image'
module.exports = gstore.model('Image', imageSchema);
