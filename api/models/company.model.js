/**
 * @version 1.0.0
 * @author Sebastian Cardona Loaiza <sebastian.cardona@gruponetw.com>
 * @copyright 2020 Todos los derechos reservados.
 */

/**
* @controller Modelo de compañia
* @description Se configura el modelo de compañia. Se configura la tabla NoSQL de la base de datos para el esquema.
*/

//Dependencias
const { instances } = require('gstore-node');

// Recupera la instancia para gstore
const gstore = instances.get('unique-id');
const { Schema } = gstore;

/**
 * Creando el esquema para el modelo de compañia
*/
const companySchema = new Schema({
    company: { type: String, required: true },
    description: { type: String, required: true }
});

/**
 * Configuración para ordenar alfabeticamente
 */
const listSettings = {
    order: { property: 'company' }
};
companySchema.queries('list');

//Exporto el esquema de base de datos como 'company'
module.exports = gstore.model('Company', companySchema);
