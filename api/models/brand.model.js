/**
 * @version 1.0.0
 * @author Sebastian Cardona Loaiza <sebastian.cardona@gruponetw.com>
 * @copyright 2020 Todos los derechos reservados.
 */

/**
* @controller Modelo de marca
* @description Se configura el modelo de marca. Se configura la tabla NoSQL de la base de datos para el esquema.
*/

//Dependencias
const { instances } = require('gstore-node');

// Recupera la instancia para gstore
const gstore = instances.get('unique-id');
const { Schema } = gstore;

/**
 * Creando el esquema para el modelo de marca
*/
const brandSchema = new Schema({
    name: { type: String, required: true },
    folder: { type: String, required: true },
    active: { type: Boolean, required: true }
});

/**
 * Configuración para ordenar alfabeticamente
 */
const listSettings = {
    order: { property: 'name' }
};
brandSchema.queries('list');

//Exporto el esquema de base de datos como 'Brand'
module.exports = gstore.model('Brand', brandSchema);
