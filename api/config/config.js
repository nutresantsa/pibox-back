module.exports = {
  'key_dev': 'nutresa-app-567b6de9794c.json',
  'bucketName_dev': 'nutresastore',
  'front_dev': 'http://localhost:4200',
  'keyPath': './api/config/key/',
  'apiStorage': 'https://storage.googleapis.com',
  'version': '1.0.0',
  'secret': 'netwconsulting_nutresa_banco_imagenes_api*'
};

module.exports.configJWT = {
  issuer: 'NetWconsulting',
  subject: 'santiago.gonzalez@netwconsulting.com',
  audience: 'http://www.netwconsulting.com',
  expiresIn: "12h",
  algorithm: 'HS256'
};

module.exports.basicAuth = {
  userDoc: "piboxdoc",
  passDoc: "1234"
}
